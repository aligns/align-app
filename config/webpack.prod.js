const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const { merge } = require("webpack-merge")
const common = require("./webpack.common.js")
module.exports = merge(common, {
  mode: "production",
  output: {
    //publicPath: "/blog/examples/",
  },
  plugins: [new BundleAnalyzerPlugin({ analyzerMode: "static" })],
})
