const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const { merge } = require("webpack-merge")
const common = require("./webpack.common.js")
module.exports = merge(common, {
  mode: "development",
  devtool: "inline-source-map", // 创建source-map映射文件
  devServer: {
    static: "../dist",
    hot: true,
    historyApiFallback: true,
    // historyApiFallback: {
    //   rewrites: [
    //     {
    //       from: /./,
    //       to: "/blog/example/",
    //     },
    //   ],
    // },
  },
})
