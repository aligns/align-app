import { type RouteObject } from "react-router-dom"
import Home from "@/pages"
export default [
  {
    path: "/",
    index: true,
    Component: Home,
  },
] as RouteObject[]
