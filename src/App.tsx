import React from "react"
import routes from "./routes"
import { ThemeProvider } from "@mui/system"
import { ConfigProvider } from "antd"
import zhCN from "antd/locale/zh_CN"
import {
  createBrowserRouter,
  createHashRouter,
  RouterProvider,
} from "react-router-dom"
import theme from "./theme"
const router = createBrowserRouter(
  routes,
  process.env.NODE_ENV == "production"
    ? { basename: "" }
    : undefined,
)
//const router = createBrowserRouter(routes, { basename: "/blog/example/" })

export default () => (
  <ConfigProvider locale={zhCN}>
    <ThemeProvider theme={theme}>
      <RouterProvider router={router}></RouterProvider>
    </ThemeProvider>
  </ConfigProvider>
)
